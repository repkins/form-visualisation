export interface FormMarkup {
  widgets: FormWidget[];
}

export interface FormWidget {
  type: 'form';
  id: number;
  name: string;
  items: FormSection[];
}

export interface FormSection {
  type: 'section';
  header: string;
  columns: number;
  items: FormFields[];
}

export interface FormFieldInput extends AbstractFormField {
  type: 'input';
  value: string;
}
export interface FormFieldCurrency extends AbstractFormField {
  type: 'currency';
  value: number;
  symbol: string;
  precision: number;
}

export interface AbstractFormField {
  type: any;
  label: string;
  required?: boolean;
}


export type FormFields = FormFieldInput | FormFieldCurrency;
