import { Component } from '@angular/core';

import { FormWidget, FormMarkup } from './widgets/form.widget';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  formWidgets: FormWidget[];

  async onFileChange(e: Event) {
    const file = this.getFileFromEvent(e);

    if (file) {
      const contents = await this.readFile(file);
      const formMarkup = this.parseJson<FormMarkup>(contents);

      this.formWidgets = formMarkup.widgets;
    }
  }

  onNumberInputChange(e: Event, precision: number) {
    const eTarget = e.target as HTMLInputElement;

    eTarget.value = this.toPrecision(eTarget.value, precision);
  }

  toPrecision(value: string, precision: number) {
    return parseFloat(value).toFixed(precision);
  }

  mask(precision: number) {

    return (rawValue = '') => {
      const rawValueLength = rawValue.length;
      const splitted = rawValue.split(' ', 2);

      const prefix = splitted[0];
      const value = splitted[1];

      const prefixMask = [ /[A-Z]/, /[A-Z]/, /[A-Z]/, ' ' ];

      if (rawValueLength === 0 || (rawValueLength > 0 && !value)) {
        return [ ...prefixMask, /[0-9]/ ];
      } else {

        const splittedValue = value.split('.', 2);
        const basePart = splittedValue[0];
        const decimalPart = splittedValue[1];

        const prefixBaseMask = [ ...prefixMask, ...new Array(basePart.length - 1).fill(/[0-9]/) ];

        if (decimalPart === undefined || precision === 0) {
          if (precision === 0) {
            return [ ...prefixBaseMask, /[0-9]/ ];
          } else {
            return [ ...prefixBaseMask, /[0-9.]/ ];
          }
        } else {
          return [ ...prefixBaseMask, /[0-9]/, '.', ...new Array(precision).fill(/[0-9]/) ];
        }
      }
    };
  }

  // private

  private getFileFromEvent(e: Event) {
    const eTarget = e.target as HTMLInputElement;

    if (eTarget.files && eTarget.files.length > 0) {
      const file = eTarget.files[0];

      return file;
    } else {
      return null;
    }
  }

  private readFile(file: File) {
    return new Promise<string>((resolve, reject) => {
      const reader = new FileReader();

      reader.onload = () => {
        resolve(reader.result);
      };
      reader.onerror = err => {
        reject(err);
      };

      reader.readAsText(file);
    });
  }

  private parseJson<T>(json: string): T {
    return JSON.parse(json);
  }
}
